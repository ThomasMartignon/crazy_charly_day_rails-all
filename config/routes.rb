Rails.application.routes.draw do
  get 'offres/offre'

  devise_for :utilisateurs
  get 'utilisateurs/home'
  get 'utilisateurs/myAccount'
  get 'utilisateurs/publicAccount'
  get 'offres/allOffres'
  get 'offres/singleOffre'
  get 'trajet/viewTrajet'
  get 'trajet/nouveauTrajet'
  get 'trajet/allTrajets'
  
  post 'trajet/nouveauTrajet' => 'trajet#create_trajet'

  get 'offres/creaOffres' => 'offres#creaOffres'
  post 'offres/creaOffres' => 'offres#crea_Offres'

  get 'offres/singleOffre/:id' => 'offres#singleOffre'

  root 'utilisateurs#home'

  devise_scope :utilisateur do
  get 'utilisateurs/sign_in', to: 'devise/sessions#new'
  post 'utilisateurs/sign_up', to: 'devise/session#create'

  get 'utilisateurs/inscription', to: 'devise/registrations#new'

  delete 'utilisateurs/profil', to: 'devise/registrations#destroy'

  get 'offres/recherche' => 'offres#recherche', as: 'recherche_offre'
  get 'utilisateurs/sign_out', to: 'devise/sessions#destroy'

  delete 'utilisateurs/sign_out', to: 'devise/sessions#destroy'
end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
