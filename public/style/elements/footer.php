<footer class="footer-area section-padding-80-0">
        <div class="container">
            <div class="row justify-content-between">

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-80">
                        <!-- Widget Title -->
                        <h4 class="widget-title">Handi Intérim</h4>

                        <!-- Footer Content -->
                        <div class="footer-content mb-15">
                            <h3>06 63 78 65 81</h3>
                            <p>julien.falentin@gmail.com</p>
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-80">
                        <!-- Widget Title -->
                        <h4 class="widget-title">Accès rapides</h4>

                        <!-- Nav -->
                        <nav>
                            <ul class="our-link">
                                <li><a href="index.php">Présentation</a></li>
                                <li><a href="offres.php">Offres d'emploi</a></li>
                                <li><a href="nouvelleOffre.php">Nouvelle offre</a></li>
                                <li><a href="monCompte.php">Mon compte</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-80">
                        <!-- Widget Title -->
                        <h4 class="widget-title">Resources</h4>

                        <!-- Nav -->
                        <nav>
                            <ul class="our-link">
                                <li><a href="#">Mentions légales</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-80">
                        <!-- Widget Title -->
                        <h4 class="widget-title">À propos du site</h4>
                        <p>Integer vehicula mauris libero, at molestie eros imperdiet sit amet.</p>

                        <!-- Copywrite Text -->
                        <div class="copywrite-text mb-30">
                            <p>&copy; Copyright 2019 <a href="#">Colorlib</a>.</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </footer>