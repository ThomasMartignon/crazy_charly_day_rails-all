# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190207192357) do

  create_table "annonces", force: :cascade do |t|
    t.integer  "utilisateur_id"
    t.integer  "categorie_id"
    t.datetime "dateExpiration"
    t.text     "descritif"
    t.datetime "dateDebut"
    t.string   "typeContrat"
    t.integer  "trajet_id"
    t.string   "nom"
    t.string   "nomOrganisme"
    t.string   "adresse"
    t.integer  "codePostal"
    t.string   "ville"
    t.text     "courteDescr"
  end

  add_index "annonces", ["categorie_id"], name: "index_annonces_on_categorie_id"
  add_index "annonces", ["trajet_id"], name: "index_annonces_on_trajet_id"
  add_index "annonces", ["utilisateur_id"], name: "index_annonces_on_utilisateur_id"

  create_table "categories", force: :cascade do |t|
    t.string "titre"
  end

  create_table "datetrajets", force: :cascade do |t|
    t.integer  "trajet_id"
    t.integer  "utilisateur_id"
    t.datetime "alle"
    t.datetime "retour"
  end

  add_index "datetrajets", ["trajet_id"], name: "index_datetrajets_on_trajet_id"
  add_index "datetrajets", ["utilisateur_id"], name: "index_datetrajets_on_utilisateur_id"

  create_table "favories", force: :cascade do |t|
    t.integer "utilisateur_id"
    t.integer "annonce_id"
  end

  add_index "favories", ["annonce_id"], name: "index_favories_on_annonce_id"
  add_index "favories", ["utilisateur_id"], name: "index_favories_on_utilisateur_id"

  create_table "passages", force: :cascade do |t|
    t.integer "utilisateur_id"
    t.integer "trajet_id"
  end

  add_index "passages", ["trajet_id"], name: "index_passages_on_trajet_id"
  add_index "passages", ["utilisateur_id"], name: "index_passages_on_utilisateur_id"

  create_table "postules", force: :cascade do |t|
    t.integer "annonce_id"
    t.integer "utilisateur_id"
    t.string  "cv_data"
    t.string  "lm_data"
    t.text    "descrition"
  end

  add_index "postules", ["annonce_id"], name: "index_postules_on_annonce_id"
  add_index "postules", ["utilisateur_id"], name: "index_postules_on_utilisateur_id"

  create_table "trajets", force: :cascade do |t|
    t.integer  "annonce_id"
    t.integer  "date_id"
    t.integer  "demande_id"
    t.integer  "passage_id"
    t.string   "destination"
    t.string   "typeVehicule"
    t.datetime "dateDebut"
    t.datetime "dateFin"
  end

  add_index "trajets", ["annonce_id"], name: "index_trajets_on_annonce_id"
  add_index "trajets", ["date_id"], name: "index_trajets_on_date_id"
  add_index "trajets", ["demande_id"], name: "index_trajets_on_demande_id"
  add_index "trajets", ["passage_id"], name: "index_trajets_on_passage_id"

  create_table "utilisateurs", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "nom"
    t.string   "prenom"
    t.string   "sexe"
    t.datetime "dateNaiss"
    t.integer  "tel"
    t.string   "adresse"
    t.integer  "codePostal"
    t.string   "ville"
    t.text     "description"
    t.integer  "ban"
    t.text     "cv_data"
    t.string   "addressePro"
  end

  add_index "utilisateurs", ["email"], name: "index_utilisateurs_on_email", unique: true
  add_index "utilisateurs", ["reset_password_token"], name: "index_utilisateurs_on_reset_password_token", unique: true

end
