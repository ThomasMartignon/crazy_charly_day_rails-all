class ChangerOffre < ActiveRecord::Migration
  def change
    add_column :annonces, :nom, :string
    add_column :annonces, :nomOrganisme, :string
    add_column :annonces, :adresse, :string
    add_column :annonces, :codePostal, :integer
    add_column :annonces, :ville, :string
    add_column :annonces, :courteDescr, :text
  end
end
