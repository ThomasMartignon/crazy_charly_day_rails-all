class AddColonnesUtilisateurs < ActiveRecord::Migration
  def change
    add_column :utilisateurs, :nom, :string
    add_column :utilisateurs, :prenom, :string
    add_column :utilisateurs, :sexe, :string
    add_column :utilisateurs, :dateNaiss, :datetime
    add_column :utilisateurs, :tel, :integer
    add_column :utilisateurs, :adresse, :string
    add_column :utilisateurs, :codePostal, :integer
    add_column :utilisateurs, :ville, :string

    add_column :utilisateurs, :description, :text
    add_column :utilisateurs, :ban, :integer
    
    add_column :utilisateurs, :cv_data, :text
  end
end
