class ModifNomDate < ActiveRecord::Migration
  def change

    drop_table :dates
    create_table :datetrajets
    add_column :datetrajets, :trajet_id, :integer
    add_column :datetrajets, :utilisateur_id, :integer
    add_column :datetrajets, :alle, :datetime
    add_column :datetrajets, :retour, :datetime
    add_index :datetrajets, :trajet_id
    add_index :datetrajets, :utilisateur_id

  end
end
