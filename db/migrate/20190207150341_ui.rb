class Ui < ActiveRecord::Migration
  def change

    create_table :annonces
    add_column :annonces, :utilisateur_id, :integer
    add_column :annonces, :categorie_id, :integer
    add_column :annonces, :dateExpiration, :datetime
    add_column :annonces, :descritif, :text
    add_column :annonces, :dateDebut, :datetime
    add_column :annonces, :typeContrat, :string
    add_column :annonces, :trajet_id, :integer
    add_index :annonces, :utilisateur_id
    add_index :annonces, :categorie_id
    add_index :annonces, :trajet_id


    create_table :postules
    add_column :postules, :annonce_id, :integer
    add_column :postules, :utilisateur_id, :integer
    add_column :postules, :cv_data, :string
    add_column :postules, :lm_data, :string
    add_column :postules, :descrition,  :text
    add_index :postules, :annonce_id
    add_index :postules, :utilisateur_id



    create_table :trajets
    add_column :trajets, :annonce_id, :integer
    add_column :trajets, :date_id, :integer
    add_column :trajets, :demande_id, :integer
    add_column :trajets, :passage_id, :integer
    add_column :trajets, :destination, :string
    add_column :trajets, :typeVehicule, :string
    add_index :trajets, :annonce_id
    add_index :trajets, :date_id
    add_index :trajets, :demande_id
    add_index :trajets, :passage_id



    create_table :dates
    add_column :dates, :trajet_id, :integer
    add_column :dates, :utilisateur_id, :integer
    add_column :dates, :alle, :datetime
    add_column :dates, :retour, :datetime
    add_index :dates, :trajet_id
    add_index :dates, :utilisateur_id



    create_table :favories
    add_column :favories, :utilisateur_id, :integer
    add_column :favories, :annonce_id, :integer
    add_index :favories, :utilisateur_id
    add_index :favories, :annonce_id


    create_table :categories
    add_column :categories, :type, :string

    create_table :passages
    add_column :passages, :utilisateur_id, :integer
    add_column :passages, :trajet_id, :integer
    add_index :passages, :utilisateur_id
    add_index :passages, :trajet_id

    add_column :utilisateurs, :addressePro, :string

  end
end
