class AddColonnesDatesTrajet < ActiveRecord::Migration
  def change
    add_column :trajets, :dateDebut, :datetime
    add_column :trajets, :dateFin, :datetime
  end
end
