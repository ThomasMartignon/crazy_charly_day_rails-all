
class OffresController < ApplicationController
  skip_before_filter :verify_authenticity_token
  def offre

  end

  def crea_Offres
    if current_utilisateur
      annonce = Annonce.new
      annonce.utilisateur_id = current_utilisateur.id
      annonce.nom = params[:nomOffre]
      annonce.nomOrganisme = params[:nomOrganisme]
      annonce.categorie_id = params[:categorie_id]

      annonce.adresse = params[:adresse]
      annonce.courteDescr = params[:courteDescr]
      annonce.descritif = params[:descritif]
      annonce.dateExpiration = params[:dateExpiration]
      annonce.dateDebut = params[:dateDebut]

      annonce.typeContrat = params[:typeContrat]
      annonce.codePostal = params[:codePostal]
      annonce.ville = params[:ville]



      if annonce.valid?
        annonce.save
        flash[:info] = "L'annonce a été créée"

        redirect_to "/utilisateurs/home"
      else
        flash[:info] = "Il manque des informations"
        redirect_to "/offre/creaOffres"
      end
    end
  end

  def allOffres
    @annonces = Annonce.all
  end

  def recherche
    @annonce = Annonce.where(["nom LIKE ?", "%#{params[:search]}%"])
  end


  def  updateOffre
    Annonce.find(params[]).update categorie_id = params[:categorie_id],
                                  dateExpiration = params[:dateExpiration],
                                  descritif = params[:descritif],
                                  dateDebut = params[:dateDebut],
                                  typeContrat = params[:typeContrat]
    redirect_to(:back)

  end

  def destroyOffre
    Annonce.find(params[:id]).destroy
    flash[:info] = "L'annonce est supprimée"
    redirect_to root_path
  end

  def creaOffres
    @liste = Categorie.all
  end

  def singleOffre
    @annonceSolo = Annonce.find(params[:id])
  end
end
