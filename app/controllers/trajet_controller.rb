class TrajetController < ApplicationController
  skip_before_filter :verify_authenticity_token
  def create_trajet
    if current_utilisateur
      trajet = Trajet.new
      trajet.annonce_id = params[:annonce_id]
      trajet.dateDebut = params[:dateDebut]
      trajet.dateFin = params[:dateFin]
      trajet.destination = params[:destination]
      trajet.typeVehicule = params[:typeVehicule]

      if trajet.valid?
        trajet.save
        flash[:info] = "Le trajet a été créé"

      end

      redirect_to "/utilisateurs/home"
    else
      flash[:info] = "Il manque des informations"
      redirect_to "/trajet/nouveauTrajet"
    end
  end
end
