class Utilisateur < ActiveRecord::Base

  has_many :annonces
  has_many :dates
  has_many :demandes
  has_many :passages

  belongs_to :annonce
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable
end
