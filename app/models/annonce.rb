class Annonce < ActiveRecord::Base

  belongs_to :utilisateur
  belongs_to :trajet
  belongs_to :categorie

  has_many :postules
  has_many :favories

end
