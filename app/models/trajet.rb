class Trajet < ActiveRecord::Base

  has_many :dates
  has_many :demandes
  has_many :passages

  belongs_to :annonce
  
end
